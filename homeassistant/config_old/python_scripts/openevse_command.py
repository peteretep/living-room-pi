# openevse_command
# Map openevse commands to a single command topic so they work like HA expects them too and like Shellies/Sonoff/ESPhome.

# Fake main

ATTR_ID = 'id'
ATTR_CMD = 'command'
ATTR_PARAMS = 'command_params'
ATTR_TOPIC='topic'
ATTR_PAYLOAD='payload'

openevse_id = data.get(ATTR_ID)
openevse_command = data.get(ATTR_CMD)
openevse_params = data.get(ATTR_PARAMS)
topic=data.get(ATTR_TOPIC)
payload=data.get(ATTR_PAYLOAD)

logger.warning("Received message on OpenEVSE command - topic: "+topic)
logger.warning("Received message on OpenEVSE command - payload: "+payload)

openevse_id=topic.split('/')[-2]
openevse_command=topic.split('/')[-1]
openevse_command_params=payload

def send_command(openevse_id,command,params,logger,hass):
    logger.info("Sending openevse to command topic: "+"openevse-{}/rapi/in/${}".format(openevse_id,command)+str(params))
    service_data = {
        'topic': "openevse-{}/rapi/in/${}".format(openevse_id,command),
        'payload': params,
        'retain': False,
        'qos': 0
    }
    hass.services.call('mqtt', 'publish', service_data, False)

if openevse_command=='set':
    if openevse_command_params=='ON':
        send_command(openevse_id,'FE','',logger,hass)
    elif openevse_command_params=='OFF':
        send_command(openevse_id,'FS','',logger,hass)
else:
    logger.error('OpenEVSE command not supported by command topic!')
    
