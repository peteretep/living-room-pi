"""Binary sensor platform for uk carbon intensity"""
import logging
from datetime import timedelta

import requests

from . import const
from homeassistant.components.binary_sensor import BinarySensorDevice
from homeassistant.config_entries import ConfigEntry
from homeassistant.helpers.typing import HomeAssistantType

_LOGGER = logging.getLogger(__name__)

SCAN_INTERVAL = timedelta(minutes=1)


async def async_setup_entry(
    hass: HomeAssistantType, entry: ConfigEntry, async_add_devices
) -> bool:
    outcode = entry.data["outcode"]
    async_add_devices([UkCarbonIntensityBinarySensor(hass, outcode)])
    return True


class UkCarbonIntensityBinarySensor(BinarySensorDevice):
    def __init__(self, hass, outcode):
        self._name = const.BINARY_SENSOR_NAME
        self._state = False
        self._attributes = {}
        self._outcode = outcode

    @property
    def name(self):
        return self._name

    @property
    def is_on(self):
        return self._state

    @property
    def device_state_attributes(self):
        return self._attributes

    def update(self):
        _LOGGER.info(f"outcode is {self._outcode}")
        if self._outcode:
            response = requests.get(
                f"https://api.carbonintensity.org.uk/regional/postcode/{self._outcode}"
            )
            response.raise_for_status()
            intensity = response.json()["data"][0]["data"][0]["intensity"]

        else:
            _LOGGER.warning("Can't find your outcode so giving national details")
            response = requests.get("https://api.carbonintensity.org.uk/intensity")
            response.raise_for_status()
            intensity = response.json()["data"][0]["intensity"]

        index = intensity[const.INDEX]
        self._attributes = {
            const.OUTCODE: self._outcode,
            const.INDEX: index,
            const.FORECAST: intensity[const.FORECAST],
        }
        if index in const.LOW_VALUES:
            self._state = True
        else:
            self._state = False
